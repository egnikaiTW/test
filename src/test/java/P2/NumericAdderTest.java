package P2;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumericAdderTest {
    @Test
    public void twoAndTwoShouldAddUptoFour() {
        NumericAdder adder = new NumericAdder();
        assertEquals(4, adder.add(2, 2));
    }

    @Test
    public void twoAndThreeShouldAddUptoFive() {
        NumericAdder adder = new NumericAdder();
        assertEquals(5, adder.add(2, 3));
    }
}