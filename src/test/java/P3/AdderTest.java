package P3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AdderTest {
    @Test
    public void twoAndTwoShouldAddUptoFour() {
        Adder adder = new Adder();
        assertEquals(4, adder.add(2, 2));
    }

    @Test
    public void twoAndThreeShouldAddUptoFive() {
        Adder adder = new Adder();
        assertEquals(5, adder.add(2, 3));
    }
}