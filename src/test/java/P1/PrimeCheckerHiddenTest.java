package P1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimeCheckerHiddenTest {
    @Test
    public void shouldDetectFiveAsPrime() {
        PrimeChecker checker = new PrimeChecker();
        assertEquals(true, checker.isPrime(5));
    }
}
