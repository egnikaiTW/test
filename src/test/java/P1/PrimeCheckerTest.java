package P1;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimeCheckerTest {
    @Test
    public void shouldDetectThreeAsPrimeNumber() {
        PrimeChecker checker = new PrimeChecker();
       assertEquals(true, checker.isPrime(3));
    }

    @Test
    public void shouldNotDetectFourAsPrimeNumber() {
        PrimeChecker checker = new PrimeChecker();
        assertEquals(false, checker.isPrime(4));
    }
}